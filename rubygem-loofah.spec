%global gem_name loofah

Name:                rubygem-%{gem_name}
Version:             2.18.0
Release:             2
Summary:             Manipulate and transform HTML/XML documents and fragments
License:             MIT
URL:                 https://github.com/flavorjones/loofah
Source0:             https://rubygems.org/gems/%{gem_name}-%{version}.gem
# git clone https://github.com/flavorjones/loofah.git && cd loofah
# git archive -v -o loofah-2.10.0-test.tar.gz v2.10.0 test/
Source1:             https://github.com/flavorjones/loofah/archive/refs/tags/v2.18.0.tar.gz
Patch0:             support-libxml2-2.10.4-backported.patch
BuildRequires:       ruby(release) rubygems-devel rubygem(nokogiri) >= 1.6.6.2 rubygem(minitest)
BuildRequires:       rubygem(crass) rubygem(rr) ruby
BuildArch:           noarch
%description
Loofah is a general library for manipulating and transforming HTML/XML
documents and fragments. It's built on top of Nokogiri and libxml2, so
it's fast and has a nice API.
Loofah excels at HTML sanitization (XSS prevention). It includes some
nice HTML sanitizers, which are based on HTML5lib's whitelist, so it
most likely won't make your codes less secure.

%package doc
Summary:             Documentation for %{name}
Requires:            %{name} = %{version}-%{release}
BuildArch:           noarch
%description doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version} -b1
%patch0 -p1

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
cp -a %{_builddir}/%{gem_name}-%{version}/test .
ruby -Itest -e 'Dir.glob "./test/**/test_*.rb", &method(:require)'
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/MIT-LICENSE.txt
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/README.md
%doc %{gem_instdir}/SECURITY.md

%changelog
* Mon Aug 14 2023 zouzhimin <zouzhimin@kylinos.cn> - 2.18.0-2
- support libxml2-2.10.4 backported

* Thu Jul 14 2022 Ge Wang <wangge20@h-partners.com> - 2.18.0-1
- update to 2.18.0

* Thu Mar 03 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 2.10.0-1
- update to 2.10.0

* Sat Sep 5 2020 yanan li <liyanan032@huawei.com> - 2.2.3-2
- Fix build fail

* Tue Aug 18 2020 geyanan <geyanan2@huawei.com> - 2.2.3-1
- package init
